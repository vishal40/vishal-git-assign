import csv

class DataEntry():
    
    def __init__(self, products):
        self.products = products
    
    def get_input(self):

        data = {}
        product_name = input("Enter the product name: ")
        print("product name===",product_name)
        
        try:
            year_purchased = int(input("Enter the year purchased: "))
            print("year_purchased===",year_purchased)
        except:
            print("This is not a whole number.")
            return data
            
        person_using = input("Name of the person using the product: ")
        print("person_using ===",person_using)
        
        location_options = ['home', 'HOME', 'Home', 'OFFICE', 'office', 'Office']
        location = input("Where it is been kept: ")
        if not location in location_options:
            print("Enter location home or office only")
            return data    
        print("location ===", location)
        
        phone_no = input("phone number: ")
        emailid = input("email id: ")
        
        
        if product_name != "" and year_purchased != "" and person_using != "" and location !="" and phone_no != "" and emailid != "":
            data['product_name'] = product_name
            data['year_purchased'] = int(year_purchased)
            data['person_using'] = person_using
            data['location'] = location
            data['phone_no'] = phone_no
            data['email_id'] = emailid
        
        return data
    
    def enter_another_record(self):
        
        options = ['Y', 'y', 'N', 'n']
        status = input("Do you wish to enter another record (Y/N)")
        if status in options:
            return status
        else:
            self.enter_another_record()
    
    def save_data_into_csv(self):
        
        with open('data.csv', 'w',) as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(['Product Name', 'Year', 'Person Name', 'Location'])
            for prod in self.products:
                writer.writerow([prod['product_name'], prod['year_purchased'], prod['person_using'], prod['location']])

    def load_contents(self):
        try:
            with open('data.csv') as csv_file:
                csv_reader = csv.reader(csv_file, delimiter=',')
                line_count = 0
                for row in csv_reader:
                    if line_count == 0:
                        print(f'{", ".join(row)}')
                        line_count += 1
                    else:
                        print(f'{row[0]}  {row[1]}  {row[2]} {row[3]}.')
                        line_count += 1
            return True
        except:
            return False
    
    def enter_product_details(self):
        
        data = self.get_input()
        if data:
            self.products.append(data)
            self.save_data_into_csv()
        
        do_further = self.enter_another_record()

        if do_further == "Y":
            self.enter_product_details()
        elif do_further == "N":
            i = 1
            for pd in self.products:
                print(str(i) + ". " + pd['product_name'] + " " + str(pd['year_purchased']) + " - " + pd['person_using'] + ", " +  pd['location'])
                i = i + 1
                print("Thank You")

products = list()
de = DataEntry(products)
de.load_contents()
de.enter_product_details()

    